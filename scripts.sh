# #!/bin/bash
# existe=$(kubectl get ingress -n gusthavo-desafio08 | wc -l)
# if [  $existe != "0" ]
# then
#     echo -e "Existe um ingress, continue\n"
#     green_status=$(kubectl describe ingress -n gusthavo-desafio08 example-ingress | grep /healthcheck2 | awk {'print $2'} | cut -d: -f1 |sed 's/ *$//g')
#     echo -e "O container que queremos e o $green_status\n"
#     if [  $green_status  = 'healthcheck2' ]
#     then
#         #fazer o deploy do green ( o deployment e o green xD)
#         echo -e "Realizando deploy do green\n"
#         envsubst < deployment-green.yaml | kubectl apply -f - --record
#         kubectl apply -f service-green.yaml
#     else
#         #fazer o deploy do green ( o deployment e o blue xD)
#         echo -e "Realizando deploy do blue\n"
#         envsubst < deployment.yaml | kubectl apply -f - --record
#         kubectl apply -f service.yaml
#     fi
# else
#     echo -e "Nao existe um ingress, criando...\n"
#     echo -e "Criando ambiente do green\n"
#     envsubst < deployment-green.yaml | kubectl apply -f - --record
#     kubectl apply -f ingress-green.yaml
#     kubectl apply -f service-green.yaml
#     echo
#     echo -e "Criando ambiente do blue\n"
#     envsubst < deployment.yaml | kubectl apply -f - --record
#     kubectl apply -f ingress.yaml
#     kubectl apply -f service.yaml
# fi

#!/bin/bash
validacao=$(kubectl get ingress -n gusthavo-desafio08| wc -l)

if test "$validacao" = "0"; then
    echo "Criando rota"
    kubectl apply -f ingress-green.yaml
    kubectl apply -f service-green.yaml
    envsubst < deployment-green.yaml | kubectl apply -f - --record
    sleep 15
    echo -e "\nVerificando\n"
    kubectl get all -n gusthavo-desafio08
    curl -k "http://a8332c4fb87194c4eab336c445f95499-5e8052a9db694ce5.elb.us-east-1.amazonaws.com/healthcheck2"
    echo -e "\nSucesso"
fi

codigo_http=$(curl --write-out %{http_code} --silent --output /dev/null http://a8332c4fb87194c4eab336c445f95499-5e8052a9db694ce5.elb.us-east-1.amazonaws.com/healthcheck2)

if [ $codigo_http != 200 ]; then
    echo "Alterando rota"
    kubectl apply -f ingress.yaml
    kubectl apply -f service.yaml
    envsubst < deployment.yaml | kubectl apply -f - --record
    kubectl delete service -n gusthavo-desafio08 green
    kubectl delete deploy -n gusthavo-desafio08 green
    sleep 15
    kubectl get all -n gusthavo-desafio08
    echo -e "\nTestando rota nova rota\n"
    curl -k "http://a8332c4fb87194c4eab336c445f95499-5e8052a9db694ce5.elb.us-east-1.amazonaws.com/healthcheck"
    echo -e "\nSucesso"
fi
