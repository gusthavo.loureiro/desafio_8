region = "us-east-1"
prefix_name = "gusthavo"
environment = "treino"
product = "teste-gusthavo"
responsible = "gusthavo.loureiro"
vpc_id = "vpc-093d98d5ecbab6dc0"
dns_name_suffix = ".treino"
node_desired_capacity = 3
node_max_capacity = 10
node_min_capacity = 1
node_instance_type = "t3a.medium"
node_root_volume_size_gb = 30
subnet_ids = [
  "subnet-0dacd2f4ed921adac",
  "subnet-06a0f201c63ae6e2d"
]
database_engine = "aurora"
database_engine_version = "5.7.mysql_aurora.2.08.1"
database_single_instance = true
database_single_instance_schemas = []
rds_instances = ["teste"]
map_accounts = [ ]
map_roles = [ ]
map_users = [ ]