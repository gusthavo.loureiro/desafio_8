FROM python:3
COPY . /app
RUN pip3 install flask && pip3 install mysql-connector-python && pip3 install redis && pip3 install flask-redis && pip3 install flask-mysql && pip3 install boto3 && pip3 install newrelic
ENV NEW_RELIC_LOG=stdout
ENV NEW_RELIC_DISTRIBUTED_TRACING_ENABLED=true
ENV NEW_RELIC_APP_NAME="nrapi"
ENV NEW_RELIC_LICENSE_KEY="7752c212f6c2bb5d63ed5d39576bd1943b11NRAL"
WORKDIR /app
CMD python3 app.py
EXPOSE 5000