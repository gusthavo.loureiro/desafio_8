#  Desafio 8

Nesse desafio, vamos utilizar o framework Well-Architected da AWS para amarrar tudo o que aprendemos nos desafios anteriores.

## Atividade 1
* Estude sobre o framework Well-Architected
* Faça o deploy da sua api utilizando apenas um pipeline com multiplos estágios.

## Critérios de aceite / sucesso
* estágio do pipelne para realizar o deploy da infra 
* estágio do pipelne para realização o deploy do S.O
* estágio do pipelne para realização o deploy da aplicação (utilizar a sua API)
* estágio do pipelne para a realização de um teste de carga


## Referências

AWS Well-Architected - https://aws.amazon.com/pt/architecture/well-architected/

Curso sobre Well-Architected: https://www.aws.training/Details/Curriculum?id=42037
