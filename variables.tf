variable "region" {
    description = "Location of the cluster."
    default = "us-east-1"
}
variable "vpc_id"  {
    description = "ID of the VPC to create the cluster"
    type = string
}
variable "subnet_ids" {
  type    = list(string)
  default = [
    "subnet-0dacd2f4ed921adac",
    "subnet-06a0f201c63ae6e2d"
  ]
}